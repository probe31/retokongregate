using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;
public class Despawner : MonoBehaviour
{
    public LeanGameObjectPool Pool { get; set; }
    
    private void OnEnable()
    {
        StartCoroutine(WaitingForDestroy());
    }

    IEnumerator WaitingForDestroy()
    {
        yield return new WaitForSeconds(0.5f);
        Pool.Despawn(this.gameObject);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
