using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MazePathfinder : MonoBehaviour
{   
    public MazeGenerator mMazeGenerator;
    private Cell mStart;
    private Cell mGoal;    
    public LineRenderer mPathViz;
    AStarPathFinder<Vector2Int> mPathFinder = new AStarPathFinder<Vector2Int>();
        
    IEnumerator Coroutine_WaitForMazeGeneration()
    {
        while (!mMazeGenerator.MazeGenerationCompleted)
            yield return null;

        mStart = mMazeGenerator.mazeTiles[0, 0].Cell;
        mGoal = mStart;
        mPathViz.startWidth = 0.1f;
        mPathViz.endWidth = 0.1f;
        mPathViz.startColor = Color.red;
        mPathViz.endColor = Color.red;
        mPathFinder.HeuristicCost = GetManhattanCost;
        mPathFinder.NodeTraversalCost = GetEuclideanCost;
        mPathFinder.onChangeCurrentNode = OnChangeCurrentNode;
        mPathFinder.onAddToClosedList = OnAddToClosedList;
        mPathFinder.onAddToOpenList = OnAddToOpenList;

    }
    public void OnChangeCurrentNode(PathFinder<Vector2Int>.PathFinderNode node)
    {
        int x = node.Location.Value.x;
        int y = node.Location.Value.y;
        MazeTile mazeCell = mMazeGenerator.mazeTiles[x, y];
    }
    public void OnAddToOpenList(PathFinder<Vector2Int>.PathFinderNode node)
    {
        int x = node.Location.Value.x;
        int y = node.Location.Value.y;
        MazeTile mazeCell = mMazeGenerator.mazeTiles[x, y];
    }
    public void OnAddToClosedList(PathFinder<Vector2Int>.PathFinderNode node)
    {
        int x = node.Location.Value.x;
        int y = node.Location.Value.y;
        MazeTile mazeCell = mMazeGenerator.mazeTiles[x, y];
    }

    public float GetManhattanCost(Vector2Int a, Vector2Int b)
    {
        return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
    }

    public float GetEuclideanCost(Vector2Int a, Vector2Int b)
    {
        return GetCostBetweenTwoCells(a, b);
    }

    public float GetCostBetweenTwoCells(Vector2Int a, Vector2Int b)
    {
        return (mMazeGenerator.mazeTiles[a.x, a.y].transform.position - mMazeGenerator.mazeTiles[b.x, b.y].transform.position).magnitude;
    }
        
    public void SearchPathToExit()
    {
        StartCoroutine(Coroutine_WaitForMazeGeneration());

        if (!mMazeGenerator.MazeGenerationCompleted)
            return;

        mGoal = mMazeGenerator.ExiMazetCell.Cell;
        FindPath();
    }

   
    public void FindPath()
    {
        mPathFinder.Initialize(mStart, mGoal);
        StartCoroutine(Coroutine_FindPathSteps());
        mPathViz.positionCount = 0;       
    }

   
    IEnumerator Coroutine_FindPathSteps()
    {
        while (mPathFinder.Status == PathFinderStatus.RUNNING)
        {
            mPathFinder.Step();
            yield return new WaitForSeconds(0f);
        }

        if (mPathFinder.Status == PathFinderStatus.SUCCESS)
        {
            OnPathFound();
        }
        else if (mPathFinder.Status == PathFinderStatus.FAILURE)
        {
            OnPathNotFound();
        }
    }

    public void OnPathFound()
    {
        
        PathFinder<Vector2Int>.PathFinderNode node = null;
        node = mPathFinder.CurrentNode;
        List<Vector3> reverse_positions = new List<Vector3>();

        while (node != null)
        {
            Vector3 pos = mMazeGenerator.mazeTiles[node.Location.Value.x, node.Location.Value.y].transform.position;
            reverse_positions.Add(pos);            
            node = node.Parent;
        }

        mPathViz.gameObject.SetActive(true);
        mPathViz.positionCount = reverse_positions.Count;
        
        for (int i = reverse_positions.Count - 1; i >= 0; i--)
        {
            Vector3 p = reverse_positions[i];
            mPathViz.SetPosition(i, new Vector3(p.x, p.y+0.05f, p.z));
        }


        mStart = mGoal;
    }

    void OnPathNotFound()
    {
        Debug.Log("Cannot find path to destination");
    }

}
