using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public GameObject player;
    public void GoToSpawn()
    {
        this.player.transform.position = this.transform.position;
    }

    
}
