using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VegetationGenerator : MonoBehaviour
{
    public List<GameObject> prefabs;

    private void Awake()
    {
        Generate();
    }

    public void Generate()
    {
        int count = Random.Range(1, 4);
        GameObject vegetation = GameObject.Instantiate(prefabs[Random.Range(0, prefabs.Count)], this.transform);
        vegetation.transform.position = new Vector3(0, 0, 0);
        
        
    }
}
