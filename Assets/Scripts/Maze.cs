using System.Collections.Generic;
using System;

public class Maze
{
    private int rows;
    private int cols;
    private Cell[,] cells;
    public int NumRows { get { return rows; } }
    public int NumCols { get { return cols; } }
    public Maze(int rows, int cols)
    {
        this.rows = rows;
        this.cols = cols;
        cells = new Cell[this.cols, this.rows];
        for (var i = 0; i < this.cols; i++)
        {
            for (var j = 0; j < this.rows; j++)
            {
                cells[i, j] = new Cell(i, j, this);
            }
        }
    }
    public Cell GetCell(int i, int j)
    {
        return cells[i, j];
    }
    public int GetCellCount()
    {
        return rows * cols;
    }
    public List<Tuple<Directions, Cell>> GetNeighboursNotVisited(int cx, int cy)
    {
        List<Tuple<Directions, Cell>> neighbours = new List<Tuple<Directions, Cell>>();
        foreach (Directions dir in Enum.GetValues(typeof(Directions)))
        {
            int x = cx;
            int y = cy;

            switch (dir)
            {
                case Directions.UP:
                    if (y < rows - 1)
                    {
                        ++y;
                        if (!cells[x, y].visited)
                        {
                            neighbours.Add(new Tuple<Directions, Cell>(Directions.UP, cells[x, y]));
                        }
                    }
                    break;
                case Directions.RIGHT:
                    if (x < cols - 1)
                    {
                        ++x;
                        if (!cells[x, y].visited)
                        {
                            neighbours.Add(new Tuple<Directions, Cell>(Directions.RIGHT, cells[x, y]));
                        }
                    }
                    break;
                case Directions.DOWN:
                    if (y > 0)
                    {
                        --y;
                        if (!cells[x, y].visited)
                        {
                            neighbours.Add(new Tuple<Directions, Cell>(Directions.DOWN, cells[x, y]));
                        }
                    }
                    break;
                case Directions.LEFT:
                    if (x > 0)
                    {
                        --x;
                        if (!cells[x, y].visited)
                        {
                            neighbours.Add(new Tuple<Directions, Cell>(Directions.LEFT, cells[x, y]));
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        return neighbours;
    }
    public void RemoveCellWall(int x, int y, Directions dir)
    {
        if (dir != Directions.NONE)
        {
            Cell cell = GetCell(x, y);
            cell.SetDirFlag(dir, false);
        }

        Directions opp = Directions.NONE;
        switch (dir)
        {
            case Directions.UP:
                if (y < rows - 1)
                {
                    opp = Directions.DOWN;
                    ++y;
                }
                break;
            case Directions.RIGHT:
                if (x < cols - 1)
                {
                    opp = Directions.LEFT;
                    ++x;
                }
                break;
            case Directions.DOWN:
                if (y > 0)
                {
                    opp = Directions.UP;
                    --y;
                }
                break;
            case Directions.LEFT:
                if (x > 0)
                {
                    opp = Directions.RIGHT;
                    --x;
                }
                break;
        }

        if (opp != Directions.NONE)
        {
            Cell cell1 = GetCell(x, y);
            cell1.SetDirFlag(opp, false);
        }
    }
}
