using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class CameraSwitcher : MonoBehaviour
{
    public CinemachineVirtualCamera overviewCamera;
    public CinemachineVirtualCamera gameplayCamera;

    public void GoToOverviewCamera()
    {
        overviewCamera.Priority = 10;
        gameplayCamera.Priority = 0;
    }

    public void GoToGameplayCamera()
    {
        overviewCamera.Priority = 0;
        gameplayCamera.Priority = 10;
    }
}
