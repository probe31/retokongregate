using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;

public class MazeGenerator : MonoBehaviour
{
    public int rows = 11;
    public int cols = 11;
    public List<LeanGameObjectPool> cellPool;
    public LeanGameObjectPool gemsPool;
    public MazeTile[,] mazeTiles;
    Stack<Cell> _stack = new Stack<Cell>();
    public MazeTile ExiMazetCell { get; private set; }

    public Maze maze
    {
        get;
        private set;
    }

    public bool MazeGenerationCompleted
    {
        get;
        private set;
    } = false;

   
    public void DespawnCells()
    {
        for (int i = 0; i < cellPool.Count; i++)
        {
            cellPool[i].DespawnAll();
        }
        
    }
    public void DespawnGems()
    {
        gemsPool.DespawnAll();
    }


    private void Start()
    {
        InitializeMaze();
    }

    public void InitializeMaze()
    {
        DespawnCells();
        DespawnGems();
        _stack = new Stack<Cell>();
        MazeGenerationCompleted = false;

        int START_X = -cols / 2;
        int START_Y = -rows / 2;

        maze = new Maze(rows, cols);
        mazeTiles = new MazeTile[cols, rows];
        for (int i = 0; i < cols; ++i)
        {
            for (int j = 0; j < rows; ++j)
            {
                GameObject cellGameObject = cellPool[Random.Range(0, cellPool.Count)].Spawn(transform);                
                Cell cell = maze.GetCell(i, j);
                cell.onSetDirFlag = OnCellSetDirFlag;
                cellGameObject.transform.position = new Vector3(START_X + (cell.x*2.5f), 0, START_Y + (cell.y*2.5f));
                mazeTiles[i, j] = cellGameObject.GetComponent<MazeTile>();
                mazeTiles[i, j].ResetWalls();
                mazeTiles[i, j].Cell = cell;
                if(Random.Range(0,100)<30)
                    CreateGem(cellGameObject.transform);
            }
        }
        CreateNewMaze();
        SetExitMaze();
    }

    void SetExitMaze()
    {
        ExiMazetCell = mazeTiles[cols - 1, rows-1];
    }

    public void OnCellSetDirFlag(int x, int y, Directions dir, bool f)
    {
        mazeTiles[x, y].SetActive(dir, f);
    }

    void CreateNewMaze()
    {
        //maze.RemoveCellWall(0, 0, Directions.LEFT);
        maze.RemoveCellWall(cols - 1, rows - 1, Directions.RIGHT);
        _stack.Push(maze.GetCell(0, 0));
        Generate();
    }

    bool GenerateStep()
    {
        if (_stack.Count == 0) return true;

        Cell c = _stack.Peek();
        var neighbours = maze.GetNeighboursNotVisited(c.x, c.y);

        if (neighbours.Count != 0)
        {
            var index = 0;
            if (neighbours.Count > 1)
            {
                index = Random.Range(0, neighbours.Count);
            }
            var item = neighbours[index];
            Cell neighbour = item.Item2;
            neighbour.visited = true;
            maze.RemoveCellWall(c.x, c.y, item.Item1);
            
            _stack.Push(neighbour);
        }
        else
        {
            _stack.Pop();
        }
        return false;
    }

    void CreateGem(Transform parent)
    {
        GameObject gem = gemsPool.Spawn(parent);
        gem.transform.position = parent.position+Vector3.up*.5f;
    }


    void Generate()
    {
        bool flag = false;
        while (!flag)
        {
            flag = GenerateStep();
        }
        MazeGenerationCompleted = true;
    }

}
