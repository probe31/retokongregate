using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;
public class GetGems : MonoBehaviour
{
    int gems = 0;
    public GameObject onGetGemsVFX;
    public LeanGameObjectPool feedbackPool;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Gems"))
        {
            gems++;

            if (onGetGemsVFX)
            {                
                GameObject feedback = feedbackPool.Spawn(other.transform.position, Quaternion.identity);
                feedback.GetComponent<Despawner>().Pool = feedbackPool;
            }
            GameObject.Destroy(other.gameObject);
                
        }
    }
}
