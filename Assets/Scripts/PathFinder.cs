using System.Collections.Generic;

public enum CostFunctionType
{
    MANHATTAN,
    EUCLIDEN,
}
public enum PathFinderStatus
{
    NOT_INITIALIZED,
    SUCCESS,
    FAILURE,
    RUNNING,
}
abstract public class Node<T>
{
    public T Value { get; private set; }
    public Node(T value)
    {
        Value = value;
    }
    abstract public List<Node<T>> GetNeighbours();
}

abstract public class PathFinder<T>
{
    public delegate float CostFunction(T a, T b);
    public CostFunction HeuristicCost { get; set; }
    public CostFunction NodeTraversalCost { get; set; }
    
    public class PathFinderNode : System.IComparable<PathFinderNode>
    {
        public PathFinderNode Parent { get; set; }
        public Node<T> Location { get; private set; }
        public float Fcost { get; private set; }
        public float GCost { get; private set; }
        public float Hcost { get; private set; }

        public PathFinderNode(Node<T> location, PathFinderNode parent, float gCost, float hCost)
        {
            Location = location;
            Parent = parent;
            Hcost = hCost;
            SetGCost(gCost);
        }

        public void SetGCost(float c)
        {
            GCost = c;
            Fcost = GCost + Hcost;
        }

        public int CompareTo(PathFinderNode other)
        {
            if (other == null) return 1;
            return Fcost.CompareTo(other.Fcost);
        }
    }

    
    public PathFinderStatus Status
    {
        get;
        private set;
    } = PathFinderStatus.NOT_INITIALIZED;

    public Node<T> Start { get; private set; }
    public Node<T> Goal { get; private set; }
    public PathFinderNode CurrentNode { get; private set; }


    protected List<PathFinderNode> mOpenList = new List<PathFinderNode>();
    protected List<PathFinderNode> mClosedList = new List<PathFinderNode>();
    protected PathFinderNode GetLeastCostNode(List<PathFinderNode> myList)
    {
        int best_index = 0;
        float best_priority = myList[0].Fcost;
        for (int i = 1; i < myList.Count; i++)
        {
            if (best_priority > myList[i].Fcost)
            {
                best_priority = myList[i].Fcost;
                best_index = i;
            }
        }

        PathFinderNode n = myList[best_index];
        return n;
    }
    protected int IsInList(List<PathFinderNode> myList, T cell)
    {
        for (int i = 0; i < myList.Count; ++i)
        {
            if (EqualityComparer<T>.Default.Equals(myList[i].Location.Value, cell))
                return i;
        }
        return -1;
    }


    public delegate void DelegatePathFinderNode(PathFinderNode node);
    public DelegatePathFinderNode onChangeCurrentNode;
    public DelegatePathFinderNode onAddToOpenList;
    public DelegatePathFinderNode onAddToClosedList;
    public DelegatePathFinderNode onDestinationFound;

    public delegate void DelegateNoArgument();
    public DelegateNoArgument onStarted;
    public DelegateNoArgument onRunning;
    public DelegateNoArgument onFailure;
    public DelegateNoArgument onSuccess;

    public bool Initialize(Node<T> start, Node<T> goal)
    {
        if (Status == PathFinderStatus.RUNNING)
        {
            return false;
        }

        Reset();
        Start = start;
        Goal = goal;
        float H = HeuristicCost(Start.Value, Goal.Value);
        PathFinderNode root = new PathFinderNode(Start, null, 0f, H);
        mOpenList.Add(root);
        CurrentNode = root;
        onChangeCurrentNode?.Invoke(CurrentNode);
        onStarted?.Invoke();
        Status = PathFinderStatus.RUNNING;
        return true;
    }

    public PathFinderStatus Step()
    {
        mClosedList.Add(CurrentNode);
        onAddToClosedList?.Invoke(CurrentNode);

        if (mOpenList.Count == 0)
        {
            Status = PathFinderStatus.FAILURE;
            onFailure?.Invoke();
            return Status;
        }

        CurrentNode = GetLeastCostNode(mOpenList);
        onChangeCurrentNode?.Invoke(CurrentNode);
        mOpenList.Remove(CurrentNode);

        if (EqualityComparer<T>.Default.Equals(CurrentNode.Location.Value, Goal.Value))
        {
            Status = PathFinderStatus.SUCCESS;
            onDestinationFound?.Invoke(CurrentNode);
            onSuccess?.Invoke();
            return Status;
        }
        
        List<Node<T>> neighbours = CurrentNode.Location.GetNeighbours();

        foreach (Node<T> cell in neighbours)
        {
            AlgorithmSpecificImplementation(cell);
        }

        Status = PathFinderStatus.RUNNING;
        onRunning?.Invoke();
        return Status;
    }

    abstract protected void AlgorithmSpecificImplementation(Node<T> cell);

    protected void Reset()
    {
        if (Status == PathFinderStatus.RUNNING)
        {
            return;
        }

        CurrentNode = null;
        mOpenList.Clear();
        mClosedList.Clear();
        Status = PathFinderStatus.NOT_INITIALIZED;
    }

}


public class AStarPathFinder<T> : PathFinder<T>
{
    protected override void AlgorithmSpecificImplementation(Node<T> cell)
    {
        if (IsInList(mClosedList, cell.Value) == -1)
        {
            float G = CurrentNode.GCost + NodeTraversalCost(CurrentNode.Location.Value, cell.Value);
            float H = HeuristicCost(cell.Value, Goal.Value);

            int idOList = IsInList(mOpenList, cell.Value);
            if (idOList == -1)
            {
                PathFinderNode n = new PathFinderNode(cell, CurrentNode, G, H);
                mOpenList.Add(n);
                onAddToOpenList?.Invoke(n);
            }
            else
            {
                float oldG = mOpenList[idOList].GCost;
                if (G < oldG)
                {
                    mOpenList[idOList].Parent = CurrentNode;
                    mOpenList[idOList].SetGCost(G);
                    onAddToOpenList?.Invoke(mOpenList[idOList]);
                }
            }
        }
    }
}
