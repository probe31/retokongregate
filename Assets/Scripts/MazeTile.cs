using UnityEngine;
public class MazeTile : MonoBehaviour
{
    [SerializeField]
    GameObject[] Walls;
    public Cell Cell { get; set; }

    public void ResetWalls()
    {
        for (int i = 0; i < Walls.Length; i++)
        {
            Walls[i].SetActive(true);
        }
    }
    public void SetActive(Directions dir, bool flag)
    {
        Walls[(int)dir].SetActive(flag);
    }

    
}
